

#include <iostream>
#include<SOIL.h>
// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

#include<fstream>
#include<string>

using namespace std;

// Function prototypes
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);

// Window dimensions
const GLuint WIDTH = 800, HEIGHT = 600;


const GLchar* vertexShaderSource;
const GLchar* vs;

const GLchar* fragmentShaderSource;

GLint success;
GLchar infoLog[512];

string readfileShader(const char* filename){
	ifstream input(filename);
	if (!input.good()){
		cout << "Lol" << endl;
	}
	return std::string(istreambuf_iterator<char>(input), istreambuf_iterator<char>());
}


// The MAIN function, from here we start the application and run the game loop
int main()
{

	int width, height;
	unsigned char* image = SOIL_load_image("Hinata5.png", &width, &height, 0, SOIL_LOAD_RGB);

	string temp = readfileShader("VShaders_Texture.glsl");
	string tempf = readfileShader("FShaders_Texture.glsl");

	vertexShaderSource = temp.c_str();
	fragmentShaderSource = tempf.c_str();


	std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
	// Init GLFW
	glfwInit();
	// Set all the required options for GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);	// tell the GFLW that we are going to use Opengl 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);	// tell the GFLW that we are going to use Opengl 3.3
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	GLint nrAttributes;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
	std::cout << "Maximum nr of vertex attributes supported: " << nrAttributes << std::endl;

	// Create a GLFWwindow object that we can use for GLFW's functions
	GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "Hello Triangle", nullptr, nullptr);
	if (window == nullptr)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	// Set the required callback functions
	glfwSetKeyCallback(window, key_callback);

	
	// Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
	glewExperimental = GL_TRUE;
	// Initialize GLEW to setup the OpenGL Function pointers
	if (glewInit() != GLEW_OK)
	{
		std::cout << "Failed to initialize GLEW" << std::endl;
		return -1;
	}

	// Define the viewport dimensions
	glViewport(0, 0, WIDTH, HEIGHT);

	GLuint vertexShader;	// create a shader object
	vertexShader = glCreateShader(GL_VERTEX_SHADER);	// since we are creating a vertex shader we pass GL_VERTEX_SHADER


	/****************
	NOTE:
	glShaderSource - replace the source code of a given shader object

	1st param ------ takes the shader object
	2nd param ------ The second argument specifies how many strings we're passing as source code, which is only one.
	3rd param ------ the actual source code of the vertex shader
	4th param ------ is the array of string lenghts

	*****************/
	glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);	// attach the shader source code to our newly created vertex shader object
	glCompileShader(vertexShader);	// compile the shader object with the attach shader source code
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	}


	/****
	same explanation as the creation of the vertex shader except we just use the fragment shader source code and GL_FRAGMENT_SHADER

	*****/
	GLuint fragmentShader;
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
	glCompileShader(fragmentShader);
	if (!success)
	{
		glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
	}

	/*****************
	NOTE:
	A shader program object is the final linked version of multiple shaders combined.
	To use the recently compiled shaders we have to link them to a shader program object and then activate this shader program when rendering objects.
	The activated shader program's shaders will be used when we issue render calls

	********************/

	GLuint shaderProgram;
	shaderProgram = glCreateProgram();  // The glCreateProgram function creates a program and returns the ID reference to the newly created shader program object.

	glAttachShader(shaderProgram, vertexShader);		// the glAttachShader function attaches the compiled shader object to the shader program. In our case attach the vertex shader to our shader program
	glAttachShader(shaderProgram, fragmentShader);		// the glAttachShader function attaches the compiled shader object to the shader program. In our case attach the fragment shader to our shader program
	glLinkProgram(shaderProgram);	// links all the attach shaders into one final shader program object

	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
	}

	// activate the shader program. Every shader and rendering call after glUseProgram will now use this program object 
	//glUseProgram(shaderProgram);		// the glUseProgram sets the given program object as the current shader program.


	/*********
	NOTE:
	Dont forget to delete the shader after we linked them to the program. Cause we no longer need them anymore.

	********/
	glDeleteShader(vertexShader);		// delete the object after linking it to the program
	glDeleteShader(fragmentShader);




	// Set up vertex data (and buffer(s)) and attribute pointers
	// More attributes: now with color
	// Set up vertex data (and buffer(s)) and attribute pointers
	GLfloat vertices[] = {
		// Positions          // Colors           // Texture Coords
		0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f, // Top Right
		0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f, // Bottom Right
		-0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f, // Bottom Left
		-0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f  // Top Left 
	};

	GLuint indices[] = {  // Note that we start from 0!
		0, 1, 3,   // First Triangle
		1, 2, 3    // Second Triangle
	};

	/**********************
	NOTE:

	A vertex array object (also known as VAO) can be bound just like a vertex buffer object and any subsequent vertex attribute calls from that point on will be stored inside the VAO.
	This has the advantage that when configuring vertex attribute pointers you only have to make those calls once and whenever we want to draw the object,
	we can just bind the corresponding VAO. This makes switching between different vertex data and attribute configurations as easy as binding a different VAO.
	All the state we just set is stored inside the VAO.

	A vertex array object stores the following:
	* Calls to glEnableVertexAttribArray or glDisableVertexAttribArray.
	* Vertex attribute configurations via glVertexAttribPointer.
	* Vertex buffer objects associated with vertex attributes by calls to glVertexAttribPointer.


	*************************/

	GLuint VBO, VAO;	// create a vertex buffer object(VBO) and a vertex array object(VAO)
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);	// generates one or more multiple buffer objects


	/**************
	NOTE:
		Element Buffer Objects 
			--- An EBO is a buffer, just like a vertex buffer object, that stores indices that OpenGL uses to decide what vertices to draw

	**************/

	GLuint EBO;
	glGenBuffers(1, &EBO);

	// Bind the Vertex Array Object first, then bind and set vertex buffer(s) and attribute pointer(s).
	glBindVertexArray(VAO);

	/***************
	NOTE:	
			OpenGL has many types of buffer objects and the buffer type of a vertex buffer object is GL_ARRAY_BUFFER
	
	******************/
	glBindBuffer(GL_ARRAY_BUFFER, VBO);	// bind the newly created buffer object to GL_ARRAY_BUFFER
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);	 // bind our Element buffer object(EBO) but this time GL_ELEMENT_ARRAY_BUFFER as the buffer type.

	/************
	NOTE:
			glBufferData is a function specifically targeted to copy user-defined data into the currently bound buffer.

			1st param ---- the type of the buffer we want to copy data into
			2nd param ---- specifies the size of the data (in bytes) we want to pass to the buffer
			3rd param ---- is the actual data we want to send
			4th param ---- The fourth parameter specifies how we want the graphics card to manage the given data. This can take 3 forms: 
				
					* GL_STATIC_DRAW: the data will most likely not change at all or very rarely.
					* GL_DYNAMIC_DRAW: the data is likely to change a lot.
					* GL_STREAM_DRAW: the data will change every time it is drawn.

	*************/

	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW); // glBufferData copies the previously defined vertex into the buffers memory
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW); // Note that we're now giving GL_ELEMENT_ARRAY_BUFFER as the buffer target	



	/*************
	NOTE:
		The glVertexAttribPointer specifies how Opengl should interpret the vertex buffer data whenever a drawing call is made

		1st param ----- specifies which vertex attribute we want to configure. 
						Remember that we specified the location of the position vertex attribute in the vertex shader with layout (location = 0). 
						This sets the location of the vertex attribute to 0 and since we want to pass data to this vertex attribute, we pass in 0.

		2nd param ----- specifies the size of the vertex attribute. The vertex attribute is a vec3 so it is composed of 3 values
		3rd param ----- specifies the type of the data which is GL_FLOAT (a vec* in GLSL consists of floating point values)
		4th param ----- specifies if we want the data to be normalized. 
						If we set this to GL_TRUE all the data that has a value not between 0 (or -1 for signed data) and 1 will be mapped to those values. 
						We leave this at GL_FALSE.
		5th param ----- known as the stride and tells us the space between consecutive vertex attribute sets.
						Since the next set of position data is located exactly 3 times the size of a GLfloat away we specify that value as the stride. 
						Note that since we know that the array is tightly packed (there is no space between the next vertex attribute value) we could've also 
						specified the stride as 0 to let OpenGL determine the stride (this only works when values are tightly packed).
						http://stackoverflow.com/questions/22296510/what-does-stride-means-in-opengles - best explanation of stride

		6th param ----- This is the offset of where the position data begins in the buffer. Since the position data is at the start of the data array this value is just 0. 
	
	************/

	/*********************

	NOTE: we need to update our stride and add a new attributes since we also update our shader files
	EXPLANATION:

	The first few arguments of glVertexAttribPointer are relatively straightforward. This time we are configuring the vertex attribute on attribute location 1. 
	The color values have a size of 3 floats and we do not normalize the values.

	Since we now have two vertex attributes we have to re-calculate the stride value. To get the next attribute value (e.g. the next x component of the position vector) in the data array,
	we have to move 6 floats to the right, three for the position values and three for the color values. This gives us a stride value of 6 times the size of a float in bytes (= 24 bytes).
	Also, this time we have to specify an offset. For each vertex, the position vertex attribute is first so we declare an offset of 0. 
	The color attribute starts after the position data so the offset is 3 * sizeof(GLfloat) in bytes (= 12 bytes).
	
	
	************************/
	// Position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);	// Remember: the number 0 is the location number of the position variable declared on our Vertex Shader
	// Color attribute
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);	// Remember: the number 1 is the location number of the color variable declared on our Vertex Shader

	// TexCoord attribute
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE,
		8 * sizeof(GLfloat), 					// 8 because we have 3 coordinates, 3 color coordinates and 2 texture coordinates: 3 + 3 + 2 = 8
		(GLvoid*)(6 * sizeof(GLfloat)));		// we have 3 coordinates and 3 color coordinates thats why its 6 * the size of float 
	glEnableVertexAttribArray(2);	// Remember: the number 2 is the location number of the texture variable declared on our Vertex Shader



	//glBindBuffer(GL_ARRAY_BUFFER, 0); // Note that this is allowed, the call to glVertexAttribPointer registered VBO as the currently bound vertex buffer object so afterwards we can safely unbind

	glBindVertexArray(0); // Unbind VAO (it's always a good thing to unbind any buffer/array to prevent strange bugs)

	// Uncommenting this call will result in wireframe polygons.
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);


	// Load and create a texture 
	GLuint texture;


	/************
	NOTE:
		glGentexture()
		1st param ---- specifies the number of the textures object to be generated
		2nd param ---- specifies an array in which the generated texture is stored

	*************/
	glGenTextures(1, &texture);

	// Bind the texture
	glBindTexture(GL_TEXTURE_2D, texture); // All upcoming GL_TEXTURE_2D operations now have effect on this texture object


	// Set the texture wrapping parameters
	/*******************

	NOTE:
			glTexParameteri()
			1st param ------  specifies the texture target; we're working with 2D textures so the texture target is GL_TEXTURE_2D.
			2nd param ------ equires us to tell what option we want to set and for which texture axis. We want to configure the WRAP option and specify it for both the S and T axis.
			3rd param ------ equires us to pass in the texture wrapping mode we'd like and in this case OpenGL will set its texture wrapping option on the currently active texture with GL_MIRRORED_REPEAT. 

	Options:

	GL_REPEAT: The default behavior for textures. Repeats the texture image.
	GL_MIRRORED_REPEAT: Same as GL_REPEAT but mirrors the image with each repeat.
	GL_CLAMP_TO_EDGE: Clamps the coordinates between 0 and 1. The result is that higher coordinates become clamped to the edge, resulting in a stretched edge pattern.
	GL_CLAMP_TO_BORDER: Coordinates outside the range are now given a user-specified border color.

	********************/
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);	// Set texture wrapping to GL_REPEAT (usually basic wrapping method)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);


	/*******************
	Texture Filtering Options:

		GL_NEAREST ---- (also known as nearest neighbor filtering) is the default texture filtering method of OpenGL. When set to GL_NEAREST,
						OpenGL selects the pixel which center is closest to the texture coordinate. Below you can see 4 pixels where the cross represents the exact texture coordinate. 
						The upper-left texel has its center closest to the texture coordinate and is therefore chosen as the sampled color.
		
		GL_LINEAR ----- (also known as (bi)linear filtering) takes an interpolated value from the texture coordinate's neighboring texels, 
						approximating a color between the texels. The smaller the distance from the texture coordinate to a texel's center, the more that texel's color contributes 
						to the sampled color

	*****************************/

	// Set texture filtering parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);






	// Load image, create texture and generate mipmaps

	/*************
	ISSUES:
	You probably noticed that the texture is flipped upside-down!
	This happens because OpenGL expects the 0.0 coordinate on the y-axis to be on the bottom side of the image,
	but images usually have 0.0 at the top of the y-axis. Some image loaders like DevIL have options to reposition the y-origin during loading,
	but SOIL doesn't. SOIL does have a function called SOIL_load_OGL_texture that loads and generates a texture with a flag called SOIL_FLAG_INVERT_Y
	that solves our problem. This function does however use features that are not available in modern OpenGL so we'd have to stick with SOIL_load_image
	for now and do the texture generation ourselves. To fix this with our vertex shader we can
	We can edit the vertex shader to swap the y-coordinate automatically
	by replacing the TexCoord assignment with TexCoord = vec2(texCoord.x, 1.0f - texCoord.y);.

	**************/
	int width2, height2;
	unsigned char* image2 = SOIL_load_image("C:\\Users\\Bellchan\\Pictures\\Hinata\\Hinata5.png", &width2, &height2, 0, SOIL_LOAD_RGB);	// we are using the SOIL library here


	/*******************
	NOTE:
		glTexImage2D() -----  Generates the texture image on the currently bound texture object

		Parameters:
			
			The first argument ----- specifies the texture target; setting this to GL_TEXTURE_2D means this operation will generate a texture on the currently bound 
									 texture object at the same target (so any textures bound to targets GL_TEXTURE_1D or G_TEXTURE_3D will not be affected).
			The second argument ---- specifies the mipmap level for which we want to create a texture for if you want to set each mipmap level manually, but we'll leave it at the base level which is 0.
			The third argument ----- tells OpenGL in what kind of format we want to store the texture. Our image has only RGB values so we'll store the texture with RGB values as well.
			The 4th, 5th argument -- sets the width and height of the resulting texture. We stored those earlier when loading the image so we'll use the corresponding variables.
			The 6th argument -------- should always be 0 (some legacy stuff).
			The 7th, 8th argument --- specify the format and datatype of the source image. We loaded the image with RGB values and stored them as chars (bytes) so we'll pass in the corresponding values.
			The 9th argument -------- is the actual image data.

	**********************/
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width2, height2, 0, GL_RGB, GL_UNSIGNED_BYTE, image2);
	glGenerateMipmap(GL_TEXTURE_2D);



	SOIL_free_image_data(image2);
	glBindTexture(GL_TEXTURE_2D, 0); // Unbind texture when done, so we won't accidentily mess up our texture.



	// Game loop
	while (!glfwWindowShouldClose(window))
	{
		// Check if any events have been activiated (key pressed, mouse moved etc.) and call corresponding response functions
		glfwPollEvents();

		// Render
		// Clear the colorbuffer
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);


		// Bind Texture

		/*****
		The default texture unit for a texture is 0 which is the default active texture unit so we did not had to assign
		a location like this.

		*******/
		glBindTexture(GL_TEXTURE_2D, texture);	// uncomment this

		 glUseProgram(shaderProgram);








        glBindVertexArray(VAO);
        //glDrawArrays(GL_TRIANGLES, 0, 3);	// drawing the triangles with only just the vertices

		/*********
		NOTE:
			We use glDrawElements to indicate we want to render the triangles from an index buffer

			1st param --- specifies the mode we want to draw
			2nd param --- is the count or number of elements we'd like to draw. We specified 6 indices so we want to draw 6 vertices in total.
			3rd param ---  is the type of the indices which is of type GL_UNSIGNED_INT.
			4th param --- allows us to specify an offset in the EBO (or pass in an index array, but that is when you're not using element buffer objects), but we're just going to leave this at 0. 


		*********/
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		
        glBindVertexArray(0);

		// Swap the screen buffers
		glfwSwapBuffers(window);
	}

	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
	glDeleteBuffers(1, &EBO);

	// Terminate GLFW, clearing any resources allocated by GLFW.
	glfwTerminate();



	return 0;
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	std::cout << key << std::endl;
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
}

