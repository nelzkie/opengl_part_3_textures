#version 330 core
in vec3 ourColor;
in vec2 TexCoord;

out vec4 color;

/*************
The fragment shader should also have access to the texture object, but how do we pass the texture object to the fragment shader? 
GLSL has a built-in data-type for texture objects called a sampler that takes as a postfix the texture type we want 
e.g. sampler1D, sampler3D or in our case sampler2D. We can then add a texture to the fragment shader by simply declaring a uniform
 sampler2D that we later assign our texture to. 

**************/
uniform sampler2D ourTexture;

void main()
{
    color = texture(ourTexture, TexCoord);
}