/**************
NOTE:

	We can use the VShaders_Texture.glsl as our vertex on this one so no need to create a new one

**************/

#version 330 core
in vec3 ourColor;
in vec2 TexCoord;

out vec4 color;

/*************
The fragment shader should also have access to the texture object, but how do we pass the texture object to the fragment shader? 
GLSL has a built-in data-type for texture objects called a sampler that takes as a postfix the texture type we want 
e.g. sampler1D, sampler3D or in our case sampler2D. We can then add a texture to the fragment shader by simply declaring a uniform
 sampler2D that we later assign our texture to. 

**************/
// Texture samplers
uniform sampler2D ourTexture1;
uniform sampler2D ourTexture2;

void main()
{


/*************
EXPLANATION:
	The final output color is now the combination of two texture lookups. 
	GLSL's built-in mix function takes two values as input and linearly interpolates between them based on its third argument. 
	If the third value is 0.0 it returns the first input; if it's 1.0 it returns the second input value.
	 A value of 0.2 will return 80% of the first input color and 20% of the second input color, resulting in a mixture of both our textures. 

************/

	color = mix(texture(ourTexture1, TexCoord), texture(ourTexture2, TexCoord), 0.2);
}